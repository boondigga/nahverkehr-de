from shapely.geometry import Point, shape
from shapely.ops import transform
from rtree import index
import get_stations as gs
import get_trips as gt
import fiona
import datetime
import json
import geojson
from statistics import mean

def delete_duplicates(station_list):
    """ Filter stations to detect attributes with same ID's and drop duplicate records.

        Within the eurail stations, some IDs occur twice, where the station names are 
        different (e.g. BAD RODACH (Germany) and (RODACH(B COBURG) BAD (Germany), 
        but the stations are actually the same.

        Args:
            station_list (list): List of DB stations

        Returns:
            unique_stations (list): List containing only unique DB stations    
    """
    duplicates_check = []
    unique_stations = []
    for station in station_list:
        if station[0] not in duplicates_check: # Check if extId already exists in list to check for duplicates
            duplicates_check.append(station[0])
            unique_stations.append(station)    
        else:
            continue
    return unique_stations

def create_spatial_objects(station_list):
    """ Create spatial objects of station_list, containing a spatial index via shapely and properties
        which can be used with fiona later.

        Args:
            station_list (list): List of DB stations

        Returns:
            attributes (list): Properties of DB stations
            geometries (list): Shapely geometries of DB stations
    """
    attributes = []
    geometries = []
    for station in station_list:
        properties = {}
        properties["extId"] = station[0]
        properties["name"] = station[1]
        point = Point((float(station[2]), float(station[3])))
        attributes.append(properties)
        geometries.append(point)
    return attributes, geometries

def spatial_index(geometries):
    """ Creates a spatial index for given geometry.

        Args:
            geometries (list): List of valid shapely geometries.
        
        Returns:
            geometries_index (index): Index of given geometries
    """
    geometries_index = index.Index()
    for idx, geom in enumerate(geometries):
        geometries_index.insert(idx, geom.bounds)
    return geometries_index

def get_destination_id(attributes, geometries, geometries_index):
    """ Intersects db stops with states to check in which state the db stop is
        and returns the updated attributes with the destination ID and city name.
        If there are stations outside of the Germany shape, they will be deleted.

        Args:
            attributes (list): List of station attributes
            geometries (list): List of station geometries
            geometries_index (list): Station geometries index

        Returns:
            attributes_updated (list): Updated list of station attributes
            geometries_updated (list): Updated list of station geometries
    """
    attributes_updated = []
    geometries_updated = []
    with fiona.open("data/bundeslaender/Bundesländergrenzen_2017_Hauptstaedte.shp", "r", encoding="UTF-8") as source:
            states = list(source)
            for state in states:
                dest_name = state["properties"]["HBF"]
                dest_id = state["properties"]["extID"]
                dest_lat = state["properties"]["HBF_LAT"]
                dest_lon = state["properties"]["HBF_LON"]
                geom = shape(state["geometry"])
                #Intersect station_index and states boundary
                for result_idx in geometries_index.intersection(geom.bounds):
                    geometry = geometries[result_idx]
                    if geometry.within(geom): # If station is within state
                        attributes[result_idx]["dest_name"] = dest_name
                        attributes[result_idx]["dest_id"] = dest_id
                        attributes[result_idx]["dest_lat"] = dest_lat
                        attributes[result_idx]["dest_lon"] = dest_lon
                        attributes_updated.append(attributes[result_idx])
                        geometries_updated.append(geometry)
    count_del_objects = len(attributes) - len(attributes_updated)
    if count_del_objects > 0:
        print("---------------------------------------------------------")
        print("{} stations are deleted, because they are outside of Germany Shapefile.".format(count_del_objects))
        print("---------------------------------------------------------")
    return attributes_updated, geometries_updated

def get_trips_info(trips):
    """ Calculates travel times and switches for each trip in single eurail trip API response.
    
        Args:
            response (list): Single "Trip" object from eurail trip API response
            
        Returns:
            info (list): List containing dictionaries for every trip within API response with format
                            {"time": trip_time, "switch": switches}
    """
    info = []
    try:
        for trip in trips:
            leg_list = trip.get("LegList").get("Leg")
            count_legs = len(leg_list)
            origin = 0
            destination = 0
            for count, leg in enumerate(leg_list):
                if count_legs == 1 and count == 0: # If it's just 1 leg, use origin and destination time from leg 0
                    origin_time = leg.get("Origin").get("time")
                    destination_time = leg.get("Destination").get("time")
                    origin = get_minutes(origin_time)
                    destination = get_minutes(destination_time)
                else: # If it's more than 1 leg, use origin time from first leg and destination time from last one
                    if count == 0:
                        origin_time = leg.get("Origin").get("time")
                        origin = get_minutes(origin_time)
                    elif count == (count_legs -1):
                        destination_time = leg.get("Destination").get("time")
                        destination = get_minutes(destination_time)
            if destination > origin:
                trip_time = destination - origin
            else:
                trip_time = (1440 - origin) + destination
            info.append({"time": trip_time, "switches": len(leg_list)-1})
    except TypeError:
        info.append({"time": -1, "switches": -1})
    return info

def get_average_infos(responses):
    """ Calculate average traveltime and average switches per station.
    
        Args:
            responses (list): List of responses from eurail trips API
            
        Returns:
            average_values (dict): Dictionary with format: {extId: "times": mean_time, "switches": mean_switches}
    """
    average_values = {}
    for response in responses:
        for id, res in response.items():
            origin = id
            train_times = []
            train_switches = []
            trips = res.get("Trip")
            average_values[origin] = {"times" : -1, "switches" : -1}
            infos = get_trips_info(trips)   
            for info in infos:
                time = info.get("time")
                switches = info.get("switches")
                if time != -1:
                    train_times.append(time)
                    train_switches.append(switches)
            if train_times == []:
                train_times = [-1]
                train_switches = [-1]
            mean_time = int(mean(train_times))
            mean_switches = int(mean(train_switches))
            
            average_values[origin]["times"] = mean_time
            average_values[origin]["switches"] = mean_switches
    return average_values

def get_minutes(time_str):
    """ Calculate minutes from time string (hh:mm:ss)
        Source: https://stackoverflow.com/questions/6402812/how-to-convert-an-hmmss-time-string-to-seconds-in-pythons
    
        Args:
            time_str (str): time in format hh:mm:ss
            
        Returns:
            minutes (int)
    """
    h, m, s = time_str.split(':')
    minutes = int(h) * 60 + int(m)
    return minutes

def mean_values_stations(attributes, geometries, trips):
    """ Appends mean_times and mean_switches to attributes list.
        Deletes values of attributes and geometries list, 
        if there are no trip informations for stations available.

        Args:
            attributes (list): List of station attributes
            geometries (list): List of station geometries
            trips (list): List of API responses from trips request
    
        Returns:
            attributes_new (list): Updated list of station attributes
            geometries_new (list): Updated list of station geometries
    """
    attributes_new = []
    geometries_new = []
    info = get_average_infos(trips)
    for station, geometry in zip(attributes, geometries):
        station["mean_time"] = info[station["extId"]]["times"]
        station["mean_switches"] = info[station["extId"]]["switches"]
        if station["mean_time"] < 0 or station["mean_switches"] < 0:
            continue
        else:
            attributes_new.append(station)
            geometries_new.append(geometry)
    return attributes_new, geometries_new

def dump_geojson(attributes, geometries, name):
    """ Writes data to GeoJSON file.

        Args:
            attributes (list): List of attributes
            geometries (list): List of geometries
            name (string): Name of output file

        Returns:
            None
    """
    time = datetime.datetime.now().strftime("%Y-%m-%d-%Hh-%Mm-%Ss")
    features = []
    for attribute, geom in zip(attributes, geometries):
        feature = geojson.Feature(geometry = geom, properties = attribute)
        features.append(feature)    
    feature_collection = geojson.FeatureCollection(features)
    with open("data/api/{}_{}.geojson".format(name, time), "w", encoding="utf-8") as f:
        geojson.dump(feature_collection, f)
    print("Data written to file: 'data/api/{}_{}.geojson'".format(name, time))
    print("---------------------------------------------------------")

def dump_json(data, name):
    """ Writes data to JSON file.

        Args:
            data: In valid JSON 
            name (string): Name of output file

        Returns:
            None
    """
    time = datetime.datetime.now().strftime("%Y-%m-%d-%Hh-%Mm-%Ss")
    with open("data/api/{}_{}.json".format(name, time), 'w') as f:
        json.dump(data, f)
    print("Data written to file: 'data/api/{}_{}.json'".format(name, time))
    print("---------------------------------------------------------")

def main():
    # Set today's date
    # For requests of a specific day, it's necessary to change the date variable to this specific date
    #date = datetime.datetime.now().strftime("%Y-%m-%d")
    date = "2020-02-27"
    # Set times
    # For each station, a single request for each of the given times will be made
    # Change times variable, if other times want to be requested
    times = ["8:00", "12:00", "16:00"]
    # Get stations
    stations = gs.stations_request("data/haltestellen/haltestellen.csv")
    # Delete duplicate stations
    stations_unique = delete_duplicates(stations)
    # Create spatial objects of station list
    station_attributes, station_geometries = create_spatial_objects(stations_unique)
    # Create index for geometry
    station_geometries_idx = spatial_index(station_geometries)
    # Update station_attributes to add capital of state and it's main station ID to properties
    station_attributes, station_geometries = get_destination_id(station_attributes, station_geometries, station_geometries_idx)
    # Get trips
    trips = gt.execute_trips_request(station_attributes, date, times)
    # Write API response from trips request as JSON file
    dump_json(trips, "{}_trips".format(date))
    # Calculate mean time and mean switches for stations and update station attribute and geometries
    station_attributes, station_geometries = mean_values_stations(station_attributes, station_geometries, trips)
    # Write updated station attributes and geometries to GeoJSON file
    dump_geojson(station_attributes, station_geometries, "{}_stations".format(date))

if __name__ == '__main__':
    main()