# DB Analysis Germany

Project for module 'GIS-Programmierung'

The aim of the project is to investigate how individual railway stations in Germany are connected to the respective state capital. Using an undocumented API of the company Eurail, the average travel time as well as the average number of necessary changes on a selected day (27.02.2020) to the respective state capital are determined for each available station (within the eurail API ) in Germany. The results are then displayed in various maps - at station, county and state level.

The report (in German) and the interactive results of the analysis are represented in [project.ipynb](project.ipynb). The interactive altair plots can also be found in the [html/](html/) directory.

Install dependencies using pip:
`pip install -r requirements.txt`

The analysis can be applied to any day and time. To do so, only the variables `date` and `time` in the file [data_preparation.py](data_preparation.py) need to be adjusted. Then run `python data_preparation.py`.
The resulting GeoJSON file (stations including average travel time and train changes) is located in the path [data/api/](data/api/). To perform the visualization for the selected day, the variable `file` can be adjusted in the subchapter **Daten/Haltestellen** in the IPython notebook.

Results (without interaction):

![alt text](img/stations_time_20200227.png "Stations - Mean travel time (2020-02-27)")

![alt text](img/stations_changes_20200227.png "Stations - Mean train changes (2020-02-27)")

![alt text](img/counties_time_20200227.png "Counties - Mean travel time (2020-02-27)")

![alt text](img/counties_changes_20200227.png "Counties - Mean train changes (2020-02-27)")