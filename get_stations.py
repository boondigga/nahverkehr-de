# Asynchronous web requests are realized with help from: 
# https://hackernoon.com/how-to-run-asynchronous-web-requests-in-parallel-with-python-3-5-without-aiohttp-264dc0f8546

import asyncio
from timeit import default_timer
from url_generator import location_url_generator
import requests
import asyncio
from concurrent.futures import ThreadPoolExecutor

stations_unique = set() # Set of unique stations
responses = [] # Store responses from get_trips() function

async def get_data(function_name, urls):
    """ Function to run multiple asynchronous API requests using multithreads and async/await. 
    
        Args:
            function_name (string): name of function, which executes the API request
            urls (list): list of urls (parameter of function to be executed)
        
        Returns:
            None
    """
    with ThreadPoolExecutor(max_workers=10) as executor:
        with requests.Session() as session:
            loop = asyncio.get_event_loop() 
            tasks = []
            for url in urls:
                tasks.append(loop.run_in_executor(executor, function_name,*(session, url)))
            await asyncio.gather(*tasks)

def fetch_data(session, url):
    """ Executing GET request for given url and return response data.
    
        Args:
            session: Session object from requests library
            url (str): url
        
        Returns:
            data: Response from GET request
    """
    with session.get(url) as response:
        if response.status_code != 200:
            print("FAILURE::{} with status-code {}".format(url, response.status_code))
        else:
            print("Request has succeeded: {}".format(url))
        data = response.json()
        return data

def get_eurail_stations(session, url):
    """ Calls fetch_data(), collects station information from eurail API response 
        and adds them to stations_unique list.

        Args:
            session: Session object from requests library
            url (list): url which will be the input param in the API call

        Returns:
            stations (list): List of stations
    """
    stations = []
    try:
        response = fetch_data(session, url)
        stop_locations = response.get("stopLocationOrCoordLocation")
        for stop in stop_locations:
            stop_location = stop.get("StopLocation")
            ext_id = stop_location.get("extId")
            name = stop_location.get("name")
            lon = stop_location.get("lon")
            lat = stop_location.get("lat")
            if "(Germany)" in name:
                stations.append([ext_id, name, lon, lat])
                stations_unique.add((ext_id, name, lon, lat))
    except TypeError:
        print("TypeError appeared - Continue with next entry.....")
    return stations

def stations_request(stations_file):
    """ Execute async function to request stations from eurail API.

        Args:
            stations_file (string): Path to stations csv file
        
        Returns:
            stations_unique (set): Set of unique stations
    """
    START_TIME = default_timer()
    location_urls = location_url_generator(stations_file)
    loop = asyncio.get_event_loop()
    future = asyncio.ensure_future(get_data(get_eurail_stations, location_urls))
    loop.run_until_complete(future)
    elapsed = default_timer() - START_TIME
    time_completed_at = "{}s".format(round(elapsed,2))
    print("Completed station requests at {}".format(time_completed_at))
    print("---------------------------------------------------------")
    return stations_unique