import altair as alt

def basemap_plot(data, fill_opacity):
    """ Creates a basemap with altair of given data.
    
        Args:
            data (GeoDataFrame): Data which should be plotted
            fill_opacity (float): fillOpacity of mark_geoshape (between 0 and 1)
        
        Returns:
            basemap: Altair Map object
    """
    basemap = alt.Chart(data).mark_geoshape(
        fill="lightgray",
        fillOpacity=fill_opacity,
        stroke="black",
        strokeWidth=0.5
    ).encode(
    ).properties(
        width=600,
        height=600
    )
    return basemap

def capitals_plot(data):
    """ Creates a plot of the capitals main station of each state.

        Args:
            data (GeoDataFrame): Data of states
        
        Returns:
            capitals: Capitals as altair square objects
    """
    click = alt.selection_multi(encodings = ["y"])
    capitals = alt.Chart(data).mark_square(size=75).encode(
        longitude = "HBF_LON",
        latitude = "HBF_LAT",
        color = alt.value("royalblue"),
        tooltip = ["GEN", "HBF"]
    ).add_selection(click)
    return capitals

def station_times_plot(data, breaks, column, legend_title, title, tooltip, color_scheme):
    """ Create altair points layer for stations representing mean times.

        Args:
            data (GeoDataFrame): Data which should be plotted
            breaks(List): List of break values of classification
            column (str): Column of the Geopandas DataFrame, which will be plotted
            legend_title (str): Legend label
            title (str): Title of the plot
            tooltip (List): List of columns to display in popup window
            color_scheme (str): Color scheme (range)

        Returns:
            stations: Single stations (points) representing mean times
            legend: Interactive legend 
    """
    label = ["{} to ≤ {}".format(breaks[0], breaks[1]),
             "> {} to ≤ {}".format(breaks[1], breaks[2]),
             "> {} to ≤ {}".format(breaks[2], breaks[3]),
             "> {} to ≤ {}".format(breaks[3], breaks[4]),
             "> {} to ≤ {}".format(breaks[4], breaks[5]),
             "> {}".format(breaks[5])]
    click = alt.selection_multi(encodings = ["color"])
    stations = alt.Chart(data, title=title).mark_geoshape(
        stroke="white",
        strokeWidth=0.5
    ).encode(
        alt.Color(column, 
                  type="nominal", 
                  legend=None,
                  scale=alt.Scale(scheme=color_scheme,
                                  domain=label
                                 )),
        tooltip = tooltip
    ).transform_filter(click).add_selection(click) 
    # Legend
    legend = alt.Chart(data).mark_rect().encode(
        y=alt.Y(column, type="nominal", sort=label, axis=alt.Axis(title=legend_title)),
        color=alt.condition(click, f"{column}:N", 
                            alt.value("lightgray"), legend=None),
        size=alt.value(250)
    ).properties(
        selection=click
    )
    return stations, legend

def station_switches_plot(data, column, legend_title, title, tooltip, color_scheme):
    """ Create altair points layer for stations representing mean switches.

        Args:
            data (GeoDataFrame): Data which should be plotted
            column (str): Column of the Geopandas DataFrame, which will be plotted
            legend_title (str): Legend label
            title (str): Title of the plot
            tooltip (List): List of columns to display in popup window
            color_scheme (str): Color scheme (range)

        Returns:
            stations: Single stations (points) representing mean switches
            legend: Interactive legend 
    """
    click = alt.selection_multi(encodings = ["y"])
    stations = alt.Chart(data, title=title).mark_geoshape(
        stroke="white",
        strokeWidth=0.5,
        size=10
    ).encode(
        alt.Color(column, 
                  type="ordinal", 
                  legend=None,
                  scale=alt.Scale(scheme=color_scheme)),
        tooltip = tooltip
    ).transform_filter(click).add_selection(click)
    # Legend
    legend = alt.Chart(data).mark_rect().encode(
        y=alt.Y(f"{column}:O", axis=alt.Axis(title=legend_title)),
        color=alt.condition(click, f"{column}:O", 
                            alt.value("lightgray"), legend=None),
        size=alt.value(250)
    ).properties(
        selection=click
    )
    return stations, legend

def counties_plot(data, column, legend_title, title, tooltip, color_scheme, labels):
    """ Creates a choropleth map with altair of given data for counties.
        Source: https://medium.com/dataexplorations/creating-choropleth-maps-in-altair-eeb7085779a1
        
        Args:
            data (GeoDataFrame): Geopandas DataFrame
            column (str): Column of the Geopandas DataFrame, which will be plotted
            legend_title (str): Legend label
            title (str): Title of the plot
            tooltip (List): List of columns to display in popup window
            color_scheme (str): Color scheme (range)
            labels (List): List of Class names 
        
        Returns:
            no_data: Altair map object representing "No Data" counties
            counties: Altair map object representing counties with values
    """
    click = alt.selection_multi(encodings = ["y"])
    # Basemap
    no_data = alt.Chart(data, title=title).mark_geoshape(
        stroke="gray",
        strokeWidth=1,
        fill="gray"
    ).encode(
        tooltip=tooltip
    ).properties(
        width=600,
        height=600
    ).add_selection(click)
    # Choropleth layer
    counties = alt.Chart(data).mark_geoshape(
        stroke="white",
        strokeWidth=0.5
    ).encode(
        alt.Color(column, 
                  type="nominal",
                  legend=alt.Legend(title=legend_title),
                  scale=alt.Scale(scheme="orangered",
                                  domain=labels
                                 ))
    )
    return no_data + counties

def states_plot(data, column, legend_title, title, tooltip, color_scheme, labels):
    """ Creates a choropleth map with altair of given data for states.
        Source: https://medium.com/dataexplorations/creating-choropleth-maps-in-altair-eeb7085779a1
        
        Args:
            data (GeoDataFrame): Geopandas DataFrame
            column (str): Column of the Geopandas DataFrame, which will be plotted
            legend_title (str): Legend label
            title (str): Title of the plot
            tooltip (List): List of columns to display in popup window
            color_scheme (str): Color scheme (range)
            labels (List): List of Class names 
        
        Returns:
            no_data: Altair map object representing "No Data" counties
            states: Altair map object representing counties with values
    """
    click = alt.selection_multi(encodings = ["y"])
    # Choropleth layer
    counties = alt.Chart(data, title=title).mark_geoshape(
        stroke="white",
        strokeWidth=0.5
    ).encode(
        alt.Color(column, 
                  type="nominal",
                  legend=alt.Legend(title=legend_title),
                  scale=alt.Scale(scheme="orangered",
                                    domain=labels)),
        tooltip=tooltip
    ).add_selection(click)
    return counties