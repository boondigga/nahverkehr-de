import csv

def location_url_generator(filepath):
    """ Generate a list containing urls based on the given csv file (offical DB stations) for requesting similar stations
        from the eurail API.
        
        Args:
            filepath (str): Path for the input csv file
            
        Returns:
            urls (list): Containing urls
    """
    urls = []
    with open(filepath, encoding="UTF-8") as source:
        reader = csv.reader(source, delimiter=";")
        next(reader)
        for row in reader:
            url = "https://api.eurail.com/timetable/location.name?input={}".format(row[1])
            urls.append(url)
    return urls

def trip_url_generator(attributes, date, departure_times):
    """ Generate a list containing dictionaries ("extId", "url") based on the given geojson file (eurail stations) 
        for a given date and times.
        
        Args:
            attributes (list): Station attributes
            date (str): Date in format HH-MM-DD
            departure_times (list): List of departure times in format HH:MM
            
        Returns:
            urls (list): Containing dictionaries with format: {"extId": extId ,"url": url}
    
    """
    urls = []
    for attribute in attributes:
        origin = attribute["extId"]
        destination = attribute["dest_id"]
        for departure in departure_times:
            url = "https://api.eurail.com/timetable/trip?lang=en&originId={}&destId={}&date={}&time={}&via=%3B".format(
                origin, destination, date, departure
            )
            url_dict = {
                "extId" : origin,
                "url" : url
            }
            urls.append(url_dict)
    return urls