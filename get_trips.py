# Asynchronous web requests are realized with help from: 
# https://hackernoon.com/how-to-run-asynchronous-web-requests-in-parallel-with-python-3-5-without-aiohttp-264dc0f8546

import asyncio
import requests
from concurrent.futures import ThreadPoolExecutor
from statistics import mean
from timeit import default_timer
from url_generator import trip_url_generator

response_list = [] # Store responses from get_trips() function

async def get_data(function_name, urls):
    """ Function to run multiple asynchronous API requests using multithreads and async/await. 
    
        Args:
            function_name (string): name of function, which executes the API request
            urls (list): list of urls (parameter of function to be executed)
        
        Returns:
            None
    """
    with ThreadPoolExecutor(max_workers=10) as executor:
        with requests.Session() as session:
            loop = asyncio.get_event_loop() 
            tasks = []
            for url in urls:
                tasks.append(loop.run_in_executor(executor, function_name,*(session, url)))
            results = await asyncio.gather(*tasks)
            for res in results:
                response_list.append(res)

def fetch_data(session, url):
    """ Executing GET request for given url and return response data.
    
        Args:
            session: Session object from requests library
            url (str): url
        
        Returns:
            data: Response from GET request
    """
    with session.get(url) as response:
        if response.status_code != 200:
            print("FAILURE::{} with status-code {}".format(url, response.status_code))
        else:
            print("Request has succeeded: {}".format(url))
        data = response.json()
        return data

def get_trips(session, url_dict):
    """ Calls fetch_data() and returns a list containing dictionaries with station ID as key 
        and API response as value.
    
        Args:
            session: Session object from requests library
            url_dict (dict): Dictionary with format: {"extId": extId, "url": url}
            
        Returns:
            response_dict (dict): Dictionary with format: {extId: url}
    
    """
    url = url_dict.get("url")
    origin = url_dict.get("extId")
    response = fetch_data(session, url)
    response_dict = {origin: response}
    return response_dict

def execute_trips_request(stations, date, times):
    """ Execute async function to request trips from eurail API.

        Args:
            attributes (list): Station attributes
            date (string): Date in format "YYYY-MM-DD"
            times (list): List of times in format "HH:MM" as strings
        
        Returns:
            response_list (set): Set of unique stations
    """
    START_TIME = default_timer()
    trip_urls = trip_url_generator(stations, date, times)
    loop = asyncio.get_event_loop()
    future = asyncio.ensure_future(get_data(get_trips, trip_urls))
    loop.run_until_complete(future)
    elapsed = default_timer() - START_TIME
    time_completed_at = "{}s".format(round(elapsed,2))
    print("Completed trips requests at: {}".format(time_completed_at))
    print("---------------------------------------------------------")
    return response_list